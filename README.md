# quant

#### 介绍
A quantitative financial investment program

#### 软件架构
软件架构说明


#### 安装教程

1. 新建anaconda虚拟环境并激活，安装jqdatasdk

   ```
   conda create -n quant python=3.6
   conda activate quant
   pip install jqdatasdk
   ```

2. 注册聚宽 ( joinquant ) 账号，地址为https://www.joinquant.com/

3. 选择第1步中的虚拟环境，将根目录下的 main.py 的第16,17行的 “聚宽账号” 和 “聚宽密码” 改成第2步中自己注册的账号密码。

#### 使用说明

1. 运行根目录下的 main.py 

2. 可以修改的选项：

   (1) my_delay ： 计算N天的收盘价涨幅；

   (2) start_date, end_date : 测试数据的开始时间和结束时间；

   (3) use_index, index_ls, index_etf_ls : 使用哪些指数的数据。需注意一一对应；
   
#### 运行截图
![avatar](./result/510300_510500_510050_delay=20.png)

#### 特别声明	

​	股市有风险，入市需谨慎！！！

​	请勿将此程序直接用于真实环境的实际投资，由此产生的一切后果本人概不负责。