from datetime import datetime,timedelta
import pandas as pd

class Context:
    def __init__(self,currency,asset=0,now_have={},target_have=[]):
        """
        initialize
        :param currency:
        :param asset: (float) 股票/基金等资产的价值
        :param now_have: (dict) key=基金代码,value={‘position’:持仓量,'price':持仓价格,'dt':买入时间}
        :param target_have: (list) 目标仓位 [基金代码1, 基金代码2, 。。。]
        """
        self.total = currency+asset
        self.currency = currency
        self.asset = asset
        self.now_have = now_have
        self.target_have = target_have