import matplotlib.pyplot as plt
from matplotlib.widgets import Cursor

class Visualization:
    def plot(self,x,y,etf_df,use_index,delay=20):
        etf_df = etf_df.fillna(value=0)
        plt.figure(figsize=(18, 9), dpi=100)
        plt.plot(x,y,label='my_total')
        for i_col in etf_df.columns:
            plt.plot(x,etf_df[i_col]/etf_df[i_col].iloc[0],label=i_col)
        plt.legend(loc='best') #显示图例
        plt.xticks(range(0,len(x),22),rotation=270) # x坐标每10个显示一次，竖着显示
        ax = plt.gca()
        cursor = Cursor(ax, horizOn=False, vertOn=True, useblit=False, color='grey', linewidth=1, linestyle='--')
        plt.grid()
        use_col = [etf_df.columns[i] for i in use_index]
        temp_ls = [i[:-5] for i in use_col]
        temp_ls.append('delay='+str(delay))
        plt.title(' '.join(temp_ls))
        plt.savefig('./result/'+'_'.join(temp_ls)+'.png')
        plt.show()